package com.gildedrose;

import com.gildedrose.item.SellableItem;
import com.gildedrose.strategy.*;

class GildedRose {
    private SellableItem[] items;

    public GildedRose(SellableItem[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (SellableItem item: getItems()){
            ItemResult result = ItemStrategyFactory.create(item.getName()).update(item);

            item.setSellIn(result.getSellIn());
            item.setQuality(result.getQualtiy());
        }
    }

    public SellableItem[] getItems() {
        return items;
    }
}