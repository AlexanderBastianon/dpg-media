package com.gildedrose.strategy;

import com.gildedrose.item.SellableItem;

public class SulfurasUpdatableItemStrategy implements UpdatableItemStrategy {

    @Override
    public ItemResult update(final SellableItem item) {
        return new ItemResult(item.getQuality(), item.getSellIn());
    }
}
