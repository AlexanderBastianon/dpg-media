package com.gildedrose.strategy;

import com.gildedrose.item.SellableItem;

import static com.gildedrose.strategy.ItemUtil.increaseQuality;
import static com.gildedrose.strategy.ItemUtil.isSellDatePassed;

public final class AgedBrieUpdatableItemStrategy implements UpdatableItemStrategy {

    private static final int DEFAULT_INCREASE = 1;

    private static final int QUALITY_MULTIPLIER = 2;

    @Override
    public ItemResult update(final SellableItem item) {
        final int sellIn = item.getSellIn() - 1;
        final int quality = increaseQuality(item.getQuality(), determineIncreaseQuality(sellIn));

        return new ItemResult(quality, sellIn);
    }

    private int determineIncreaseQuality(final int sellIn){
        return isSellDatePassed(sellIn) ? DEFAULT_INCREASE * QUALITY_MULTIPLIER : DEFAULT_INCREASE;
    }
}
