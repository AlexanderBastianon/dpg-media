package com.gildedrose.strategy;

import static com.gildedrose.item.ItemConstants.*;

public class ItemStrategyFactory {

    private final static SulfurasUpdatableItemStrategy SULFURAS_ITEM_STRATEGY = new SulfurasUpdatableItemStrategy();
    private final static AgedBrieUpdatableItemStrategy AGED_BRIE_ITEM_STRATEGY = new AgedBrieUpdatableItemStrategy();
    private final static BackStageUpdatableItemStrategy BACK_STAGE_ITEM_STRATEGY = new BackStageUpdatableItemStrategy();
    private final static ConjuredUpdatableItemStrategy CONJURED_ITEM_STRATEGY = new ConjuredUpdatableItemStrategy();
    private final static DefaultUpdatableItemStrategy DEFAULT_ITEM_STRATEGY = new DefaultUpdatableItemStrategy();

    public static UpdatableItemStrategy create(final String name) {
        switch (name) {
            case SULFURAS:
                return SULFURAS_ITEM_STRATEGY;
            case AGED_BRIE:
                return AGED_BRIE_ITEM_STRATEGY;
            case BACKSTAGE_PASSES:
                return BACK_STAGE_ITEM_STRATEGY;
            case CONJURED:
                return CONJURED_ITEM_STRATEGY;
            default:
                return DEFAULT_ITEM_STRATEGY;
        }
    }
}
