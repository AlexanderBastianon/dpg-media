package com.gildedrose.strategy;

public class ItemResult {

    private final int qualtiy;
    private final int sellIn;

    public ItemResult(final int qualtiy, final int sellIn) {
        this.qualtiy = qualtiy;
        this.sellIn = sellIn;
    }

    public int getQualtiy() {
        return qualtiy;
    }

    public int getSellIn() {
        return sellIn;
    }

}
