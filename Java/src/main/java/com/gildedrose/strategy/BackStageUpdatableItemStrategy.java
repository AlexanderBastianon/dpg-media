package com.gildedrose.strategy;

import com.gildedrose.item.SellableItem;

import static com.gildedrose.strategy.ItemUtil.*;

public final class BackStageUpdatableItemStrategy implements UpdatableItemStrategy {

    private static final int FIVE_DAYS = 5;
    private static final int TEN_DAYS = 10;

    private static final int DEFAULT_INCREASE = 1;
    private static final int INCREASE_TEN_DAYS = 2;
    private static final int INCREASE_FIVE_DAYS = 3;

    @Override
    public ItemResult update(final SellableItem item) {
        final int quality = increaseQuality(item.getQuality(), determineIncreaseQuality(item.getSellIn()));
        final int sellIn = item.getSellIn() - 1;

        return new ItemResult(ItemUtil.isSellDatePassed(sellIn) ? 0 : quality, sellIn);
    }

    private int determineIncreaseQuality(final int sellIn) {
        final int quality;

        if (sellIn <= FIVE_DAYS) {
            quality = INCREASE_FIVE_DAYS;
        } else if (sellIn <= TEN_DAYS) {
            quality = INCREASE_TEN_DAYS;
        } else {
            quality = DEFAULT_INCREASE;
        }

        return quality;
    }
}
