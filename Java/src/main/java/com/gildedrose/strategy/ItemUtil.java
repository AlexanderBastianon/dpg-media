package com.gildedrose.strategy;

public class ItemUtil {

    private static int MAX_QUALITY = 50;
    private static int MIN_QUALITY = 0;

    /**
     * Util method to check if sellDatePassed
     * @param sellIn
     * @return true if date has passed otherwise false
     */
    public static boolean isSellDatePassed(final int sellIn){
        return sellIn < 0;
    }

    /**
     * Increases Quality but not passing te upper bound of {@link #MAX_QUALITY}
     * @param quality currect quality
     * @param increaseValue total value to increase
     * @return new calculated quality
     */
    public static int increaseQuality(final int quality, final int increaseValue) {
        if (quality < MAX_QUALITY) {
            return Math.min(quality + increaseValue, MAX_QUALITY); // sonar lint suggestion
        }

        return quality;
    }

    /**
     * Decreases Quality but not passing the lower limit of {@link #MIN_QUALITY}
     * @param quality currect quality
     * @param decreaseValue total value to decrease
     *
     * @return new calculated quality
     */
    public static int decreaseQuality(final int quality, final int decreaseValue) {
        if (quality > MIN_QUALITY) {
            return Math.max(quality - decreaseValue, MIN_QUALITY); // sonar lint suggestion
        }

        return quality;
    }
}
