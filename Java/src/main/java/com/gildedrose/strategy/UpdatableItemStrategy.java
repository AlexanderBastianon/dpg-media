package com.gildedrose.strategy;

import com.gildedrose.item.SellableItem;

public interface UpdatableItemStrategy {

    ItemResult update(final SellableItem item);

}
