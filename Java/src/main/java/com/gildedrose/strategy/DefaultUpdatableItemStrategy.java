package com.gildedrose.strategy;

import com.gildedrose.item.SellableItem;

import static com.gildedrose.strategy.ItemUtil.decreaseQuality;
import static com.gildedrose.strategy.ItemUtil.isSellDatePassed;

public class DefaultUpdatableItemStrategy implements UpdatableItemStrategy {

    private static int DEFAULT_DECREASE = 1;

    @Override
    public ItemResult update(SellableItem item) {
        final int sellIn = item.getSellIn() - 1;
        final int quality = decreaseQuality(item.getQuality(), determineDecreaseQuality(sellIn));

        return new ItemResult(quality, sellIn);
    }

    private int determineDecreaseQuality(final int sellIn) {
        return isSellDatePassed(sellIn) ? DEFAULT_DECREASE * 2 : DEFAULT_DECREASE;
    }
}
