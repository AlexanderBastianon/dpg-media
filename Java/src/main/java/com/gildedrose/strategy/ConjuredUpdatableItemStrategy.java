package com.gildedrose.strategy;

import com.gildedrose.item.SellableItem;

import static com.gildedrose.strategy.ItemUtil.decreaseQuality;
import static com.gildedrose.strategy.ItemUtil.isSellDatePassed;

final public class ConjuredUpdatableItemStrategy implements UpdatableItemStrategy {

    private static int DEFAULT_DECREASE = 2;
    private static final int QUALITY_MULTIPLIER = 2;

    @Override
    public ItemResult update(final SellableItem item) {
        final int sellIn = item.getSellIn() - 1;
        final int quality = decreaseQuality(item.getQuality(), determineDecreaseQuality(sellIn));

        return new ItemResult(quality, sellIn);
    }

    private int determineDecreaseQuality(final int sellIn) {
        return isSellDatePassed(sellIn) ? DEFAULT_DECREASE * QUALITY_MULTIPLIER : DEFAULT_DECREASE;
    }
}
