package com.gildedrose.item;

public class ItemFactory {

    public static SellableItem create(final String name, final int sellIn, final int quality) {
        return new SellableItem(name, sellIn, quality);
    }
}
