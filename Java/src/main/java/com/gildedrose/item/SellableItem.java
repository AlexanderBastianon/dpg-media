package com.gildedrose.item;

public class SellableItem {

    private final Item item;

    /**
     * Constructor for SellableItem
     * @param name of Item
     * @param sellIn of Item
     * @param quality of Item
     */
    public SellableItem(final String name, final int sellIn, final int quality){
        this.item = new Item(name, sellIn, quality);
    }

    public String getName(){
        return this.item.name;
    }

    public int getSellIn(){
        return this.item.sellIn;
    }

    public void setSellIn(final int sellIn) {
        this.item.sellIn = sellIn;
    }

    public int getQuality() {
        return this.item.quality;
    }

    public void setQuality(final int quality) {
        this.item.quality = quality;
    }

    @Override
    public String toString() {
        return this.item.name + ", " + this.item.sellIn + ", " + this.item.quality;
    }

}
