package com.gildedrose.item;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SellableItemTest {

    @Test
    void checkCorrectWrappingItem() {
        SellableItem item = new SellableItem("Default Item", 5, 10);

        assertEquals("Default Item", item.getName());
        assertEquals(5, item.getSellIn());
        assertEquals(10, item.getQuality());
    }

}