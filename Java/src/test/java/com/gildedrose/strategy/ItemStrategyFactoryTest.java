package com.gildedrose.strategy;

import com.gildedrose.item.ItemConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.gildedrose.item.ItemConstants.*;
import static org.junit.jupiter.api.Assertions.*;

class ItemStrategyFactoryTest {

    @Test
    void createDefaultType() {
        UpdatableItemStrategy actual = ItemStrategyFactory.create("Default Item");

        assertTrue(actual instanceof DefaultUpdatableItemStrategy);
    }

    @Test
    void createSulfuras() {
        UpdatableItemStrategy actual = ItemStrategyFactory.create(SULFURAS);

        assertTrue(actual instanceof SulfurasUpdatableItemStrategy);
    }

    @Test
    void createConjured() {
        UpdatableItemStrategy actual = ItemStrategyFactory.create(CONJURED);

        assertTrue(actual instanceof ConjuredUpdatableItemStrategy);
    }

    @Test
    void createAgedBrie() {
        UpdatableItemStrategy actual = ItemStrategyFactory.create(AGED_BRIE);

        assertTrue(actual instanceof AgedBrieUpdatableItemStrategy);
    }

    @Test
    void createBackstage() {
        UpdatableItemStrategy actual = ItemStrategyFactory.create(BACKSTAGE_PASSES);

        assertTrue(actual instanceof BackStageUpdatableItemStrategy);
    }
}