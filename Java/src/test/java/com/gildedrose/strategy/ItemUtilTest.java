package com.gildedrose.strategy;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemUtilTest {

    @Nested
    class SellInDate {

        @Test
        void sellInDate_positive() {
            boolean actual = ItemUtil.isSellDatePassed(0);

            assertEquals(false, actual);
        }

        @Test
        void sellInDate_negative() {
            boolean actual = ItemUtil.isSellDatePassed(-1);

            assertEquals(true, actual);
        }
    }

    @Nested
    class DecreaseQuality {
        @Test
        void decreaseQuality_normal() {
            int actual = ItemUtil.decreaseQuality(10, 4);

            assertEquals(6, actual);
        }

        @Test
        void decreaseQuality_passingLowerBounds() {
            int actual = ItemUtil.decreaseQuality(10, 12);

            assertEquals(0, actual);
        }

        @Test
        void decreaseQuality_initialQualityNegative() {
            int actual = ItemUtil.decreaseQuality(-10, 12);

            assertEquals(-10, actual);
        }
    }

    @Nested
    class IncreaseQuality {
        @Test
        void increaseQuality_normal() {
            int actual = ItemUtil.increaseQuality( 40, 5);

            assertEquals(45, actual);
        }

        @Test
        void increaseQuality_passingUpperBounds() {
            int actual = ItemUtil.increaseQuality(40,15);

            assertEquals(50, actual);
        }

        @Test
        void IncreaseQuality_initialQualityOverUpperBoundLimit() {
            int actual = ItemUtil.increaseQuality(55,15);

            assertEquals(55, actual);
        }
    }

}