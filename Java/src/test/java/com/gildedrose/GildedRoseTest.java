package com.gildedrose;

import com.gildedrose.item.ItemFactory;
import com.gildedrose.item.SellableItem;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static com.gildedrose.item.ItemConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    private static final String DEFAULT_ITEM = "Default Item";
    
    private static final int MAX_QUALITY = 50;
    private static final int MIN_QUALITY = 0;

    private static final int FIVE_DAYS = 5;
    private static final int TEN_DAYS = 10;
    private static final int TWELVE_DAYS = 12;

    @DisplayName("Default Item")
    @Nested
    class DefaultItem {

        @Test
        void normalDecrease() {
            SellableItem actual = ItemFactory.create(DEFAULT_ITEM, 4, 20);

            updateQuality(actual);

            assertEquals(DEFAULT_ITEM, actual.getName());
            assertEquals(3, actual.getSellIn());
            assertEquals(19, actual.getQuality());
        }

        @Test
        void doubleDecrease_sellDatePassed() {
            SellableItem actual = ItemFactory.create(DEFAULT_ITEM, -3, 20);

            updateQuality(actual);

            assertEquals(DEFAULT_ITEM, actual.getName());
            assertEquals(-4, actual.getSellIn());
            assertEquals(18, actual.getQuality());
        }

        @Test
        void doubleDecrease_passingMinimum() {
            SellableItem actual = ItemFactory.create(DEFAULT_ITEM, -3, 1);

            updateQuality(actual);

            assertEquals(DEFAULT_ITEM, actual.getName());
            assertEquals(-4, actual.getSellIn());
            assertEquals(0, actual.getQuality());
        }

        @Test
        void decreaseNotBelowMinimum() {
            SellableItem actual = ItemFactory.create(DEFAULT_ITEM, 5, 0);

            updateQuality(actual);

            assertEquals(DEFAULT_ITEM, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(MIN_QUALITY, actual.getQuality());
        }

        @Test
        void negativeQuality_notDecreased() {
            SellableItem actual = ItemFactory.create(DEFAULT_ITEM, 5, -10);

            updateQuality(actual);

            assertEquals(DEFAULT_ITEM, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(-10, actual.getQuality());
        }

        @Test
        void qualityMoreThanMax_normalDecrease() {
            SellableItem actual = ItemFactory.create(DEFAULT_ITEM, 5, 55);

            updateQuality(actual);

            assertEquals(DEFAULT_ITEM, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(54, actual.getQuality());
        }
    }

    @DisplayName("Sulfuras, Hand of Ragnaros")
    @Nested
    class Sulfuras {

        @Test
        void quantityAndSellInNeverChange() {
            SellableItem actual = ItemFactory.create(SULFURAS, 4, 30);

            updateQuality(actual);

            assertEquals(SULFURAS, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(30, actual.getQuality());
        }

        @Test
        void qualityCanBeMoreThanMax() {
            SellableItem actual = ItemFactory.create(SULFURAS, 4, 80);

            updateQuality(actual);

            assertEquals(SULFURAS, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(80, actual.getQuality());
        }

        @Test
        void qualityCanBeLessThanMin() {
            SellableItem actual = ItemFactory.create(SULFURAS, 4, -80);

            updateQuality(actual);

            assertEquals(SULFURAS, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(-80, actual.getQuality());
        }
    }

    @DisplayName("Aged Brie")
    @Nested
    class AgedBrie {

        @Test
        void qualityNotPassingMax() {
            SellableItem actual = ItemFactory.create(AGED_BRIE, 5, MAX_QUALITY);

            updateQuality(actual);

            assertEquals(AGED_BRIE, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(MAX_QUALITY, actual.getQuality());
        }

        @Test
        void qualityNormalIncrement() {
            SellableItem actual = ItemFactory.create(AGED_BRIE, 5, 10);

            updateQuality(actual);

            assertEquals(AGED_BRIE, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(11, actual.getQuality());
        }

        @Test
        void qualityDoubleIncrement_sellDatePassed() {
            SellableItem actual = ItemFactory.create(AGED_BRIE, -5, 10);

            updateQuality(actual);

            assertEquals(AGED_BRIE, actual.getName());
            assertEquals(-6, actual.getSellIn());
            assertEquals(12, actual.getQuality());
        }

        @Test
        void qualityDoubleIncrement_passingMax() {
            SellableItem actual = ItemFactory.create(AGED_BRIE, -5, 49);

            updateQuality(actual);

            assertEquals(AGED_BRIE, actual.getName());
            assertEquals(-6, actual.getSellIn());
            assertEquals(50, actual.getQuality());
        }

        @Test
        void qualityNormalIncrement_initialQualityBelowMin() {
            SellableItem actual = ItemFactory.create(AGED_BRIE, 5, -10);

            updateQuality(actual);

            assertEquals(AGED_BRIE, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(-9, actual.getQuality());
        }

        @Test
        void noQualityIncrement_initialQualityAboveMax() {
            SellableItem actual = ItemFactory.create(AGED_BRIE, 5, 55);

            updateQuality(actual);

            assertEquals(AGED_BRIE, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(55, actual.getQuality());
        }
    }

    @Nested
    class BackstagePasses {

        @Test
        void initialQualityAboveMax_noIncrement(){
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, TWELVE_DAYS, 55);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(11, actual.getSellIn());
            assertEquals(55, actual.getQuality());
        }

        @Test
        void initialQualityBelowMin_normalIncrement(){
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, TWELVE_DAYS, -10);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(11, actual.getSellIn());
            assertEquals(-9, actual.getQuality());
        }

        @Test
        void qualityNotPassingMax() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, FIVE_DAYS, MAX_QUALITY);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(MAX_QUALITY, actual.getQuality());
        }

        @Test
        void qualityNormalIncrement_moreThan10Days() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, TWELVE_DAYS, 40);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(11, actual.getSellIn());
            assertEquals(41, actual.getQuality());
        }

        @Test
        void qualityDoubleIncrement_lessOrEqual10Days() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, TEN_DAYS, 30);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(9, actual.getSellIn());
            assertEquals(32, actual.getQuality());
        }

        @Test
        void qualityTripleIncrement_lessOrEqual5Days() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, FIVE_DAYS, 30);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(33, actual.getQuality());
        }

        @Test
        void qualityTripleIncrement_overflowMax() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, FIVE_DAYS, 48);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(50, actual.getQuality());
        }

        @Test
        void qualityToZero_sellInDatePassed() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, 0, 48);

            updateQuality(actual);

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(-1, actual.getSellIn());
            assertEquals(0, actual.getQuality());
        }
    }

    @DisplayName("Conjured Mana Cake")
    @Nested
    class Conjured {

        @Test
        void normalDecrease() {
            SellableItem actual = ItemFactory.create(CONJURED, 4, 20);

            updateQuality(actual);

            assertEquals(CONJURED, actual.getName());
            assertEquals(3, actual.getSellIn());
            assertEquals(18, actual.getQuality());
        }

        @Test
        void doubleDecrease_sellDatePassed() {
            SellableItem actual = ItemFactory.create(CONJURED, -3, 20);

            updateQuality(actual);

            assertEquals(CONJURED, actual.getName());
            assertEquals(-4, actual.getSellIn());
            assertEquals(16, actual.getQuality());
        }

        @Test
        void doubleDecrease_passingMinimum() {
            SellableItem actual = ItemFactory.create(CONJURED, -3, 3);

            updateQuality(actual);

            assertEquals(CONJURED, actual.getName());
            assertEquals(-4, actual.getSellIn());
            assertEquals(0, actual.getQuality());
        }

        @Test
        void decreaseNotBelowMinimum() {
            SellableItem actual = ItemFactory.create(CONJURED, 5, 0);

            updateQuality(actual);

            assertEquals(CONJURED, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(MIN_QUALITY, actual.getQuality());
        }

        @Test
        void negativeQuality_notDecreased() {
            SellableItem actual = ItemFactory.create(CONJURED, 5, -10);

            updateQuality(actual);

            assertEquals(CONJURED, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(-10, actual.getQuality());
        }

        @Test
        void qualityMoreThanMax_normalDecrease() {
            SellableItem actual = ItemFactory.create(CONJURED, 5, 55);

            updateQuality(actual);

            assertEquals(CONJURED, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(53, actual.getQuality());
        }
    }

    @Nested
    class KnownCases {

        @Test
        void dexterityVest() {
            SellableItem actual = ItemFactory.create("+5 Dexterity Vest", 10, 20);

            SellableItem[] items = new SellableItem[]{actual};

            GildedRose app = new GildedRose(items);

            app.updateQuality();

            assertEquals("+5 Dexterity Vest", actual.getName());
            assertEquals(9, actual.getSellIn());
            assertEquals(19, actual.getQuality());
        }

        @Test
        void agedBrie() {
            SellableItem actual = ItemFactory.create(AGED_BRIE, 2, 0);

            SellableItem[] items = new SellableItem[]{actual};

            GildedRose app = new GildedRose(items);

            app.updateQuality();

            assertEquals(AGED_BRIE, actual.getName());
            assertEquals(1, actual.getSellIn());
            assertEquals(1, actual.getQuality());
        }

        @Test
        void sulfuras1() {
            SellableItem actual = ItemFactory.create(SULFURAS, 0, 80);

            SellableItem[] items = new SellableItem[]{actual};

            GildedRose app = new GildedRose(items);

            app.updateQuality();

            assertEquals(SULFURAS, actual.getName());
            assertEquals(0, actual.getSellIn());
            assertEquals(80, actual.getQuality());
        }

        @Test
        void sulfuras2() {
            SellableItem actual = ItemFactory.create(SULFURAS, -1, 80);

            SellableItem[] items = new SellableItem[]{actual};

            GildedRose app = new GildedRose(items);

            app.updateQuality();

            assertEquals(SULFURAS, actual.getName());
            assertEquals(-1, actual.getSellIn());
            assertEquals(80, actual.getQuality());
        }

        @Test
        void BackstagePasses() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, 15, 20);

            SellableItem[] items = new SellableItem[]{actual};

            GildedRose app = new GildedRose(items);

            app.updateQuality();

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(14, actual.getSellIn());
            assertEquals(21, actual.getQuality());
        }

        @Test
        void BackstagePasses2() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, 10, 49);

            SellableItem[] items = new SellableItem[]{actual};

            GildedRose app = new GildedRose(items);

            app.updateQuality();

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(9, actual.getSellIn());
            assertEquals(50, actual.getQuality());
        }

        @Test
        void BackstagePasses3() {
            SellableItem actual = ItemFactory.create(BACKSTAGE_PASSES, 5, 49);

            SellableItem[] items = new SellableItem[]{actual};

            GildedRose app = new GildedRose(items);

            app.updateQuality();

            assertEquals(BACKSTAGE_PASSES, actual.getName());
            assertEquals(4, actual.getSellIn());
            assertEquals(50, actual.getQuality());
        }
    }

    static void updateQuality(SellableItem... items) {
        GildedRose app = new GildedRose(items);
//        GildedRoseOriginal app = new GildedRoseOriginal(items); // run test agains original code
        app.updateQuality();
    }
}
